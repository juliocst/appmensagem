package com.example.julio.appmensagem;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.security.Permission;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Manifest;

public class MainActivity extends AppCompatActivity {

    MessageServer messageServer;
    TextView tRecipientes, tMessage;
    String recipiente = "";
    SharedPreferences sharedPref;
    final int MY_PERMISSIONS_REQUEST_SEND_SMS = 129;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int checkSMS = ContextCompat.checkSelfPermission(MainActivity.this,
                android.Manifest.permission.SEND_SMS);

        Context context = this;

        sharedPref = context.getSharedPreferences("PREF", Context.MODE_PRIVATE);

        tRecipientes = (TextView) findViewById(R.id.txtRecip);
        tMessage = (TextView) findViewById(R.id.txtMes);

        tRecipientes.setText(sharedPref.getString("Recip", ""));
        tMessage.setText(sharedPref.getString("Mens", ""));

        messageServer = new MessageServer();

    }

    @Override
    public void onPause() {

        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putString("Recip", tRecipientes.getText().toString());
        editor.putString("Mens", tMessage.getText().toString());

        editor.apply();

        super.onPause();
    }

    public void enviarLista(View v) {

        if (ContextCompat.checkSelfPermission(MainActivity.this,
                android.Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    android.Manifest.permission.SEND_SMS)) {


            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{ android.Manifest.permission.SEND_SMS },
                        MY_PERMISSIONS_REQUEST_SEND_SMS);


            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    char last;
                    recipiente = tRecipientes.getText().toString();

                    last = recipiente.charAt(recipiente.length() -1);

                    if ((last != ','))
                        recipiente = tRecipientes.getText().toString() + ",";

                    String lista[] = new String[1000];
                    int x = 0;


                    while (recipiente.contains(",")) {
                        lista[x] = recipiente.substring(0, recipiente.indexOf(",")).trim();
                        recipiente = recipiente.replace(lista[x], "");
                        recipiente = recipiente.replaceFirst(",", "");
                        x++;
                    }

                    messageServer.sendMessage(lista, tMessage.getText().toString());

                } else {
                }
            }

        }
    }

}
